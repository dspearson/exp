// Compiled by ClojureScript 1.10.520 {}
goog.provide('exp.core');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('bytebuffer');
goog.require('reagent.core');
cljs.core.println.call(null,"This text is printed from src/exp/core.cljs. Go ahead and edit it and see reloading in action.");
exp.core.multiply = (function exp$core$multiply(a,b){
return (a * b);
});
if((typeof exp !== 'undefined') && (typeof exp.core !== 'undefined') && (typeof exp.core.app_state !== 'undefined')){
} else {
exp.core.app_state = reagent.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text","text",-1790561697),"Hello world!"], null));
}
exp.core.get_app_element = (function exp$core$get_app_element(){
return goog.dom.getElement("app");
});
exp.core.hello_world = (function exp$core$hello_world(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,exp.core.app_state))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Edit this in src/exp/core.cljs and watch it change!"], null)], null);
});
exp.core.mount = (function exp$core$mount(el){
return reagent.core.render_component.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [exp.core.hello_world], null),el);
});
exp.core.mount_app_element = (function exp$core$mount_app_element(){
var temp__5457__auto__ = exp.core.get_app_element.call(null);
if(cljs.core.truth_(temp__5457__auto__)){
var el = temp__5457__auto__;
return exp.core.mount.call(null,el);
} else {
return null;
}
});
exp.core.mount_app_element.call(null);
exp.core.on_reload = (function exp$core$on_reload(){
return exp.core.mount_app_element.call(null);
});

//# sourceMappingURL=core.js.map
